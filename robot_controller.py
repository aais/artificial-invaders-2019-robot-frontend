import serial


class RobotController:
    """
    RobotController handles the serial port communication
    to Arduino
    """
    def __init__(self, serial_port, baudrate):
        """
        Open serial port with specified port and baudrate
        """
        self._ser = serial.Serial(port=serial_port, baudrate=baudrate)
        self._ser.isOpen()

    def set_motors(self, left_motor_value, right_motor_value):
        """
        Send the left and right motor values to Arduino.
        Returns True if got response back from Arduino, else False
        """
        data = 'R' + str(right_motor_value) + 'L' + str(left_motor_value) + 'G'
        print("Sending: {}".format(data))
        self._ser.write(data.encode())

        data = self._ser.readline()
        print("Received: {}".format(data))
        return bool(data)

    def close(self):
        """
        Close serial port connection
        """
        self._ser.close()
