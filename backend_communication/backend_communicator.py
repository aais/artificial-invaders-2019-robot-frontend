from multiprocessing import Process
from concurrent import futures
import time
import atexit
import grpc

from robot_controller import RobotController
import backend_communication.RobotCommunication_pb2 as rc_pb2
import backend_communication.RobotCommunication_pb2_grpc as rc_pb2_grpc


class BackendCommunicator(rc_pb2_grpc.RobotCommunicatorServicer):
    """
    Class to send the robot backend motor commands
    to the robot's motor controller via serial port
    """
    def __init__(self, parameters):
        self._server_port = parameters["server_port"]
        self._robot_controller = RobotController(
            parameters["arduino_communication"]["serial_port"],
            parameters["arduino_communication"]["serial_baudrate"])

        self._robot_server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
        rc_pb2_grpc.add_RobotCommunicatorServicer_to_server(self, self._robot_server)
        self._robot_server.add_insecure_port('[::]:{}'.format(self._server_port))

        self._process = None
        atexit.register(self.close_server)

    def _start_server(self):
        self._robot_server.start()
        print('=== Robot server running at port {} ...'.format(self._server_port))
        try:
            while True:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            self._robot_server.stop(0)
            print('Robot server Stopped ...')

    def start_server(self):
        """
        Start the gRPC server to listen to calls from
        Robot Backend
        """
        self._process = Process(target=self._start_server)
        self._process.start()

    def close_server(self):
        """
        Stop gRPC server
        """
        print('=== Closing Robot server ...')
        self._robot_controller.close()
        self._process.terminate()


    def SetMotorValues(self, request, context):
        """
        Send motor values to the robot's motors
        """
        left_motor_value = request.leftMotorValue
        right_motor_value = request.rightMotorValue
        command_ok = self._robot_controller.set_motors(
            left_motor_value, right_motor_value)
        return rc_pb2.DoneInfo(done=command_ok)
