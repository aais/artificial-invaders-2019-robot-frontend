const int CELL_1 = A0;
const int CELL_2 = A1;
const int CELL_3 = A2;

//              13, 11, 10, 9
int pinPwm[] = { 0,  0,  0, 0};

//                  R L
int motorSpeed[] = {0,0};

void motorTask()
{
    if (motorSpeed[0] >= 0)
    {
        pinPwm[0] = motorSpeed[0];
        pinPwm[1] = 0;
    }

    else if (motorSpeed[0] < 0)
    {
        pinPwm[0] = 0;
        pinPwm[1] = abs(motorSpeed[0]);
    }

    // ------------------
    
    if (motorSpeed[1] >= 0)
    {
        pinPwm[2] = 0;
        pinPwm[3] = motorSpeed[1];
    }

    else if (motorSpeed[1] < 0)
    {
        pinPwm[2] = abs(motorSpeed[1]);
        pinPwm[3] = 0;
    }

    // ------------------
    
    /*if (pinPwm[0] != pinPwm[1]) // Safety
    {
        pinPwm[0] = 0;
        pinPwm[1] = 0;
    }
    if (pinPwm[2] != pinPwm[3]) // Safety
    {
        pinPwm[2] = 0;
        pinPwm[3] = 0;
    }*/
    // analogWrite(13, pinPwm[0]); // FOR MICRO
    analogWrite(5, pinPwm[0]); // FOR NANO
    delay(1);
    analogWrite(11, pinPwm[1]);
    delay(1);
    analogWrite(10, pinPwm[2]);
    delay(1);
    analogWrite(9, pinPwm[3]);
    delay(1);

    Serial.print("pwm:");
    Serial.print(pinPwm[0]);
    Serial.print(",");
    Serial.print(pinPwm[1]);
    Serial.print(",");
    Serial.print(pinPwm[2]);
    Serial.print(",");
    Serial.print(pinPwm[3]);
    Serial.print(",");

    Serial.print("speed:");
    Serial.print(motorSpeed[0]);
    Serial.print(",");
    Serial.print(motorSpeed[1]);
    Serial.print(",");
}

void batteryTask()
{
    int c1, c2, c3;
    c1 = analogRead(CELL_1)*5; // an approximation
    c2 = analogRead(CELL_2)*5; // an approximation
    c3 = analogRead(CELL_3)*5; // an approximation

    c2 = c2*2-c1;
    c3 = c3*3-c2-c1;

    Serial.print("battery:");
    Serial.print(c1);
    Serial.print(",");
    Serial.print(c2);
    Serial.print(",");
    Serial.print(c3);

    if (true)
    {
        digitalWrite(6, HIGH);
    }
}

void setup() {
    Serial.begin(57600);
    
    // Motor Driver;
    // pinMode(13, OUTPUT); // PWM FOR MICRO
    pinMode(5, OUTPUT); // PWM FOR NANO
    pinMode(11, OUTPUT); // PWM
    pinMode(10, OUTPUT); // PWM
    pinMode(9, OUTPUT); // PWM
    pinMode(8, OUTPUT); // ?
    pinMode(7, OUTPUT); // ?
    delay(100);
    digitalWrite(8, HIGH);
    digitalWrite(7, HIGH);

    // Battery Monitor;
    pinMode(6, OUTPUT); // ?
    Serial.println("start");
}

void loop() {
    char firstChar;
    int value;

    
    if (Serial.available())
    {
        firstChar = Serial.read();

        if (firstChar == 'R' || 
            firstChar == 'L')
        {
            value = Serial.parseInt();
            if (value > 255 || value < -255)
            {
              value = 0;
            }

            switch (firstChar)
            {
                case 'R': // Right Motor
                    motorSpeed[0] = value;
                    break;
                case 'L': // Left Motor
                    motorSpeed[1] = value;
                    break;
                default:
                    break;
            }
            return;
        }
        else if(firstChar == 'C')
        {
            Serial.print("Test OK");
        }
        else if(firstChar == 'G')
        {
            motorTask();
        }

        batteryTask();
        Serial.println("");
    }
    return;
}