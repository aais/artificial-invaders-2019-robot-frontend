import serial
import time

from utils import parse_options


def main():
    """
    Test communication between Raspberry Pi and Arduino.
    """
    params = parse_options("params.yaml")
    ser = serial.Serial(
        port=params["arduino_communication"]["serial_port"],
        baudrate=params["arduino_communication"]["serial_baudrate"])
    ser.isOpen()

    count = 0
    while count < 10:
        # data = 'C'
        data = 'R' + str(150) + 'L' + str(150) + 'G'
        ser.write(data.encode())
        data = ser.readline()
        print("Got: {}".format(data))
        time.sleep(0.01)
        count += 1

if __name__ == '__main__':
    main()
