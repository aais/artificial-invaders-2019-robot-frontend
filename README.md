# On PC
## Setup a virtual environment
This repo is tested with Python 3.6.8.

Create Python virtual environment if you want for testing on a PC
### Linux
`virtualenv venv`
`source venv/bin/activate`
### Windows 10
`python -m venv venv`
`venv\Scripts\Activate`

For the robot's Raspberry Pi install the packages without virtual environment.

## Install Python packages
`pip install -r requirements.txt`

## Generate gRPC codes
### Robot communication
- Run the following command to generate the gRPC code from the .proto-file: 
    ```sh
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./backend_communication/RobotCommunication.proto
    ```
### Robot reporter
- Run the following command to generate the gRPC code from the .proto-file: 
    ```sh
    python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./robot_reporter/RobotReporter.proto
    ```

# On Arduino
## Arduino installation
- Upload `robot_arduino_controller.ino` to Arduino with Arduino's own IDE/VSCode plugin/Other method



# On Raspberry Pi
## Raspberry Pi installation
- Install [Raspbian Buster lite image](https://www.raspberrypi.org/downloads/raspbian/) to a SD-card with for example [this guide](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)
- Enable SSH by placing an empty file called `ssh` to the SD-card's partition called `boot`. Find the file from `raspberry_pi_files`-folder in this repo.
- Edit the file `wpa_supplicant.conf` at `raspberry_pi_files`-folder in this repo to have your Wifi's ssid and password. Copy this `wpa_supplicant.conf` to the SD-card's partition called `boot`.
- Start Raspberry Pi with the just installed SD-card
- Find the IP-address of the Raspberry Pi and SSH into it
- On first boot run `sudo raspi-config` and go to `7 Advanced Options -> A1 Expand Filesystem`
- Exit the program and reboot the Raspberry Pi to get all the space on the SD-card to use
- SSH into Raspberry Pi and update packages with `sudo apt update` and `sudo apt upgrade -y`
- Raspberry is now ready for robot_frontend installation.

## Raspberry Pi setup
### Install virtualenv package
- `sudo apt install python3-pip -y`
- `sudo pip3 install virtualenv`

### Copy files from PC to Raspberry Pi
- Copy the `artificial-invaders-2019-robot-frontend`-repos files from your PC to Raspberry Pi by example running this on your Linux PC `rsync -avz -e ssh --exclude-from=rsync-exclude.txt . pi@the-ip-of-rpi:/home/pi/artificial-invaders-2019-robot-frontend` in `artificial-invaders-2019-robot-frontend`-folder
In Windows 10 you can use `scp -r . pi@the-ip-of-rpi:/home/pi/artificial-invaders-2019-robot-frontend`

### Setup virtual environment
- Create Python virtual environment to Raspberry Pi with `virtualenv venv` and take it into use with `source venv/bin/activate`

### Install Python packages to Raspberry Pi
`pip install -r requirements-raspberrypi.txt`

### See which tty device Arduino is
- See that the Arduino is connected to the Raspberry Pi via USB
- Find the tty port with `ls -lA /dev/serial/by-path`
- Put the port into `params.yaml`

### Test Raspberry Pi to Arduino communication
- With Arduino connected to Raspberry Pi run `python rpi_arduino_connection_test.py`
- You should see:
    ```
    Got: b'start\r\n'
    Got: b'Test OKbattery:1870,1330,1120\r\n'
    ```

### Find interface name
- Run `ifconfig` and find the name of the Wifi interface. Most likely it is `wlan0`
- Put the interface name into `params.yaml`

### Set robot type
- Set the `robot_type` parameter in `params.yaml`

### Setup robot frontend autostart on startup
- Place `robot_frontend.service` from `raspberry_pi_files`-folder on Raspberry Pi to `/etc/systemd/system` with `sudo cp raspberry_pi_files/robot_frontend.service /etc/systemd/system`
- Set the `robot_frontend.service` to start at startup with `sudo systemctl enable robot_frontend.service`
- The `robot_frontend.service` starts `main.py` on Raspberry Pi's startup

### Manually test drive robot
- Connect Arduino with USB to your PC instead of Raspberry Pi
- Run `python manual_driver_test.py` and use keyboard to command the robot's motors

## Notes
Source of Arduino and Raspberry Pi code
https://github.com/semihiseri/invaderBot/tree/master/software
