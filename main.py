#! /home/pi/artificial-invaders-2019-robot-frontend/venv/bin/python
import time

from utils import parse_options
from backend_communication.backend_communicator import BackendCommunicator
from robot_reporter.robot_reporter_client import RobotReporter


def main():
    """
    Start backend communicator which gets
    motor values from robot backend
    """
    params = parse_options("params.yaml")

    interface_name = params["interface_name"]
    robot_reporter = RobotReporter(
        params["robot_backend"],
        interface_name)

    robot_reporter.report_to_backend(
        params["robot_type"],
        params["server_port"])

    server = BackendCommunicator(params)
    server.start_server()

    try:
        while True:
            time.sleep(60*60*60)

    except KeyboardInterrupt:
        server.close_server()
        print('Robot server Stopped ...')


if __name__ == '__main__':
    main()
