from pynput import keyboard
from pynput.keyboard import Key
import serial
import time


KEY = None

# Motor command mode.
# If mode=0 motors continue rotating if no new command is sent
# If mode=1 motors stop rotating if no new command is sent
MODE = 1


def on_press(key):
    """
    Set the global value KEY when a key is pressed
    """
    global KEY
    try:
        # print('alphanumeric key {0} pressed'.format(key.char))
        KEY = key.char
    except AttributeError:
        pass
        # print('special key {0} pressed'.format(key))


def on_release(key):
    """
    Set the global value KEY to None when key is released
    """
    global KEY
    # print('{0} released'.format(key))
    KEY = None
    if key == keyboard.Key.esc:
        # Stop listener
        return False


def main():
    """
    Test communication between Robot and your PC.
    Connect PC to Arduino via serial port and send motor
    commands according to the keypresses
    """
    global MODE, KEY
    ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600)
    ser.isOpen()

    speed = 100

    try:
        with keyboard.Listener(
                on_press=on_press,
                on_release=on_release) as listener:
            while True:
                vert = 0
                horz = 0

                if KEY == 'w':
                    vert += 1
                    print("W is pressed")
                if KEY == 'a':
                    print("A is pressed")
                    horz += 1
                if KEY == 's':
                    vert -= 1
                    print("S is pressed")
                if KEY == 'd':
                    horz -= 1
                    print("D is pressed")

                horz *= -1
                vert *= -1

                if KEY == 'h':
                    while KEY == 'h':
                        pass
                    # print("H is pressed")
                    speed += 50
                if KEY == 'y':
                    while KEY == 'y':
                        pass
                    # print("Y is pressed")
                    speed -= 50
                else:
                    pass

                if speed > 250:
                    speed = 250
                if speed < 100:
                    speed = 100

                if horz == 0:
                    left = speed*vert
                    right = speed*vert
                elif horz == 1 and vert != 0:
                    left = 0
                    right = speed*vert
                elif horz == -1 and vert != 0:
                    right = 0
                    left = speed*vert
                elif horz == 1 and vert == 0:
                    left = -speed
                    right = -left
                elif horz == -1 and vert == 0:
                    left = speed
                    right = -left

                if MODE == 1 or (right != 0 or left != 0):
                    data = 'R' + str(right) + 'L' + str(left) + 'G'
                    ser.write(data.encode())
                    data = ser.readline()
                    print("Got: {}".format(data))
                    time.sleep(0.01)

            listener.join()
    except Exception as error:
        print("====ERROR =====")
        print(error)
        print("====ERROR =====")


if __name__ == '__main__':
    main()
