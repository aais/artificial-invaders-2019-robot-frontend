"""
RobotReporter reports robot's type and
ip-address to the Robot Backend
"""
import netifaces
import grpc

import robot_reporter.RobotReporter_pb2 as rr_pb2
import robot_reporter.RobotReporter_pb2_grpc as rr_pb2_grpc
from robot_reporter.RobotReporter_pb2 import STRIKER, GOALIE


class RobotReporter:
    """
    RobotReporter reports robot's type and
    ip-address to the Robot Backend
    """
    def __init__(self, robot_backend_params, interface_name):
        self._backend_ip = robot_backend_params["ip"]
        self._backend_port = robot_backend_params["port"]
        self._interface_name = interface_name
        self._channel = grpc.insecure_channel(
            '{}:{}'.format(self._backend_ip, self._backend_port))
        self._stub = rr_pb2_grpc.RobotReporterStub(self._channel)

    def report_to_backend(self, agent_type, robot_port):
        """
        Send Robot's ip-address, robot-frontend port and
        agent type to Robot Backend
        """
        robot_ip = \
            netifaces.ifaddresses(
                self._interface_name)[netifaces.AF_INET][0]['addr']
        agent_type = STRIKER if agent_type == "striker" else GOALIE
        robot_info = rr_pb2.RobotInfo(
            ipAddress=robot_ip,
            port=str(robot_port),
            agentType=agent_type)
        self._stub.GetRobotInfo(robot_info)


def main():
    from utils import parse_options
    params = parse_options("params.yaml")

    interface_name = params["interface_name"]
    robot_reporter = RobotReporter(
        params["robot_backend"],
        interface_name)

    robot_reporter.report_to_backend(
        params["robot_type"],
        params["server_port"])


if __name__ == '__main__':
    main()
